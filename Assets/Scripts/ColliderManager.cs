﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Platformer2d;


namespace Samurai {


	/// <summary>
	/// Collider manager.
	/// 
	/// * Rotation is not supported currently
	/// </summary>
	[RequireComponent (typeof(Doll))]
	public class ColliderManager : MonoBehaviour {
		public List<Collider> colliders;
		public List<string> colliderNames;

		private Doll _doll;


		void Start () {
			if ((colliders != null && colliderNames != null) && (colliders.Count != colliderNames.Count)) {
				Debug.LogError("Collider Manager: the length of collidres and collidernNames do not match");
			}
			_doll = GetComponent<Doll>();
		}
		

		void Update () {
			tk2dSprite sprite = _doll.GetDollPart(DollPartType.Body).sprite;
			tk2dSpriteColliderDefinition[] colliderInfos = sprite.CurrentSprite.customColliders;

			for (int i=0; i<colliders.Count; i++) {
				Collider currentCollider = null;
				string name = colliderNames[i];
				bool isEnabled = false;

				foreach (tk2dSpriteColliderDefinition info in colliderInfos) {
					if (info.name.Equals(name)) {
						currentCollider = colliders[i];	
						isEnabled = true;

						if (info.type == tk2dSpriteColliderDefinition.Type.Box) {
							BoxCollider currentBoxCollider = (BoxCollider) currentCollider;
							currentBoxCollider.size = info.Size;
							currentBoxCollider.center = info.origin;
						}
						else if (info.type == tk2dSpriteColliderDefinition.Type.Circle) {
							SphereCollider currentSphereCollider = (SphereCollider) currentCollider;
							currentSphereCollider.radius = info.Radius;
							currentSphereCollider.center = info.origin;
						}
					}
				}

				colliders[i].gameObject.SetActive(isEnabled);
			}
		}
	}

}