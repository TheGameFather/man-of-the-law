﻿using UnityEngine;
using System.Collections;

public class DamageDealer : MonoBehaviour {
	public delegate void DealDamageDelegate (DamageReceiver receiver);
	public event DealDamageDelegate SignalDealDamage;


	void OnTriggerEnter (Collider other) {
		if (!other.tag.Equals(tag)) {
			DamageReceiver receiver = other.GetComponent<DamageReceiver>();
			if (SignalDealDamage != null) {
				SignalDealDamage(receiver);
			}
			receiver.TriggerEvent(this);
		}
	}
}
