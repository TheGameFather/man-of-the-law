﻿using UnityEngine;
using System.Collections;

public class DamageReceiver : MonoBehaviour {
	public delegate void DamageReceiveDelegate(DamageDealer dealer);
	public event DamageReceiveDelegate SignalDamageReceive;


	public void TriggerEvent (DamageDealer dealer) {
		if (SignalDamageReceive != null) {
			SignalDamageReceive(dealer);
		}
	}
}
