﻿using UnityEngine;
using System.Collections;

public class Fresh : MonoBehaviour {

	public ParticleSystem particle;
	public DamageReceiver receiver;
	
	void Start () {
		receiver.SignalDamageReceive += ReceiveDamage;
	}

	private void ReceiveDamage (DamageDealer dealer) {
		ParticleSystem blood = Instantiate(particle) as ParticleSystem;
		Transform trans = blood.transform;
		Vector3 pos = transform.position;
		pos.y += 0.2f;
		trans.position = pos;
		trans.rotation = particle.transform.rotation;
		blood.Play();
	}
}
