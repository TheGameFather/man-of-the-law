﻿using UnityEngine;
using System.Collections;
using FSM;
using Platformer2d;

namespace Samurai {
	public static class Helper {

		public static void InitMeleeFSMDatabase (FSMDatabase database) {
			database.SetData<Direction2d>(FSMJargon.MoveDirection, Direction2d.None);
			database.SetData<int>(FSMJargon.MeleeCount, 0);
			database.SetData<bool>(FSMJargon.TriggerJump, false);
			database.SetData<bool>(FSMJargon.isDash, false);
		}

		public static void InitUnarmedFSMDatabase (FSMDatabase database) {
			database.SetData<bool>(FSMJargon.isArmed, false);
		}
	}
}