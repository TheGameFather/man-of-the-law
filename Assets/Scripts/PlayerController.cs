using UnityEngine;
using System.Collections;
using Platformer2d;

namespace Samurai {

	public class PlayerController : MonoBehaviour {
		private Doll _doll;
		private PlatformMotor _motor;

		void Start () {
			_doll = GetComponent<Doll>();
			_motor = GetComponent<PlatformMotor>();
		}
	
		void Update () {
			string animation = SConstants.ANIMATION_UNARMED_IDLE;
			Direction2d moveDirection = Direction2d.None;
			if (Input.GetKeyDown(KeyCode.A) || Input.GetKey(KeyCode.A)) {
				moveDirection = Direction2d.Left;
			}
			else if (Input.GetKeyDown(KeyCode.D) || Input.GetKey(KeyCode.D)) {
				moveDirection = Direction2d.Right;
			}

			if (moveDirection != Direction2d.None) {
				animation = SConstants.ANIMATION_UNARMED_SPRINT;
				_doll.FlipX(moveDirection);
			}

			_motor.Move(moveDirection);


			if (Input.GetKey(KeyCode.J)) {
				animation = SConstants.ANIMATION_ATTACK_LIGHT;
			}
			else if (Input.GetKey(KeyCode.K)) {
				animation = SConstants.ANIMATION_ATTACK_HEAVY;
			}

			_doll.PlayAnimation(DollPartType.Body, animation);
		}
	}

}