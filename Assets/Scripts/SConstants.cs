﻿using UnityEngine;
using System.Collections;

namespace Samurai {

	public static class SConstants {
		public static string ANIMATION_UNARMED_IDLE = "unarmed idle";
		public static string ANIMATION_UNARMED_SPRINT = "unarmed sprint";
		public static string ANIMATION_UNARMED_JUMP_TRANSITION = "unarmed jump transition";
		public static string ANIMATION_UNARMED_JUMP = "unarmed jump";
		public static string ANIMATION_UNARMED_FALL = "unarmed fall";
		public static string ANIMATION_ATTACK_LIGHT = "light attack";
		public static string ANIMATION_ATTACK_HEAVY = "heavy attack";
		public static string ANIMATION_ARMED_IDLE = "armed idle";
		public static string ANIMATION_ARMED_WALK_FORWARD = "armed walk forward";
		public static string ANIMATION_ARMED_WALK_BACKWARD = "armed walk backward";
		public static string ANIMATION_DASH_FORWARD = "dash forward";
		public static string ANIMATION_DASH_BACKWARD = "dash backward";

	}

}