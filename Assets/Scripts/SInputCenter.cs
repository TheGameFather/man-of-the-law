﻿using UnityEngine;
using System.Collections;
using FSM;
using Platformer2d;


namespace Samurai {

	public class SInputCenter : MonoBehaviour {

		private FSMDatabase _database;

		private KeyCode _previousInput;
		private float _timer;



		void Start () {
			_database = GetComponent<FSMDatabase>();
		}


		void Update () {
			_timer += Time.deltaTime;

			UpdateArmedInput();
			UpdateMovementInput();
			UpdateAttackInput();
		}

		private void UpdateArmedInput () {
			bool isArmed = _database.GetData<bool>(FSMJargon.isArmed);

			if (Input.GetKeyDown(KeyCode.Tab)) {
				_previousInput = KeyCode.Tab;
				isArmed = !isArmed;
			}

			_database.SetData<bool>(FSMJargon.isArmed, isArmed);
		}

		private void UpdateMovementInput () {
			Direction2d moveDirection = Direction2d.None;

			KeyCode currentKey = KeyCode.None;

			if (Input.GetKeyDown(KeyCode.A) || Input.GetKey(KeyCode.A)) {
				moveDirection = Direction2d.Left;
				currentKey = KeyCode.A;
			}
			else if (Input.GetKeyDown(KeyCode.D) || Input.GetKey(KeyCode.D)) {
				moveDirection = Direction2d.Right;
				currentKey = KeyCode.D;
			}

			bool isDash = false;
			_database.SetData<Direction2d>(FSMJargon.MoveDirection, moveDirection);
			if (currentKey != KeyCode.None && currentKey == _previousInput && Input.GetKeyDown(currentKey) && _timer < 0.3f) {
				isDash = true;
				_timer = 1000;
				_previousInput = KeyCode.None;
				_database.SetData<bool>(FSMJargon.isDash, true);
			}

			if (currentKey != KeyCode.None && !isDash) {
				_timer = 0;
				_previousInput = currentKey;
			}

			bool triggerJump = false;
			if (Input.GetKeyDown(KeyCode.W)) {
				_previousInput = KeyCode.W;
				triggerJump = true;
			}

			_database.SetData<bool>(FSMJargon.TriggerJump, triggerJump);
		}

		private void UpdateAttackInput () {
			
			if (Input.GetKeyDown(KeyCode.J)) {
				_previousInput = KeyCode.J;
				int attackCount = _database.GetData<int>(FSMJargon.MeleeCount);
				_database.SetData<int>(FSMJargon.MeleeCount, ++attackCount);
			}
		}
	}

}