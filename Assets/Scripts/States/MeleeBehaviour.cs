﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using FSM;
using Platformer2d;

namespace Samurai {

	public class MeleeBehaviour : FSMBehaviour {

		protected override void Start () {
			base.Start();

			Helper.InitMeleeFSMDatabase(_database);
			_fsm = new MeleeFSM(_database);
		}
	}
}