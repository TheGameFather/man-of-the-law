﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using FSM;
using Platformer2d;

namespace Samurai {

	public class MeleeFSM : FSMMachine {
		public IdleState idleState {get; set;}
		public MoveState moveState {get; set;}
		public DashState dashState {get; set;}
		public JumpState jumpState {get; set;}
		public MeleeState meleeState {get; set;}


		public MeleeFSM (FSMDatabase database) : base () {
			idleState = new IdleState(database);
			idleState.animationPart = DollPartType.Body;
			idleState.idleAnimation = SConstants.ANIMATION_ARMED_IDLE;
			AddState(idleState);
			
			moveState = new MoveState(database);
			moveState.animationPart = DollPartType.Body;
			moveState.moveForwardAnimation = SConstants.ANIMATION_ARMED_WALK_FORWARD;
			moveState.moveBackwardAnimation = SConstants.ANIMATION_ARMED_WALK_BACKWARD;
			moveState.changeDirection = false;
			moveState.speedFactor = 0.4f;
			AddState(moveState);

			dashState = new DashState(database);
			dashState.animationPart = DollPartType.Body;
			dashState.dashForwardAnimation = SConstants.ANIMATION_DASH_FORWARD;
			dashState.dashBackwardAnimation = SConstants.ANIMATION_DASH_BACKWARD;
			AddState(dashState);
			
			jumpState = new JumpState(database);
			jumpState.animationPart = DollPartType.Body;
			jumpState.enterJumpAnimation = SConstants.ANIMATION_UNARMED_JUMP_TRANSITION;
			jumpState.jumpAnimation = SConstants.ANIMATION_UNARMED_JUMP;
			jumpState.fallAnimation = SConstants.ANIMATION_UNARMED_FALL;
			jumpState.exitJumpAnimation = SConstants.ANIMATION_UNARMED_JUMP_TRANSITION;
			jumpState.jumpForceFactor = 0.9f;
			AddState(jumpState);
			
			meleeState = new MeleeState(database);
			meleeState.animationPart = DollPartType.Body;
			meleeState.meleeAnimations = new List<string>() {SConstants.ANIMATION_ATTACK_LIGHT, SConstants.ANIMATION_ATTACK_HEAVY};
			AddState(meleeState);
			
			TransitionValue<Direction2d> moveTriggered = new TransitionValue<Direction2d>(moveState, database, FSMJargon.MoveDirection, Direction2d.None, false);
			TransitionValue<Direction2d> moveStopped = new TransitionValue<Direction2d>(idleState, database, FSMJargon.MoveDirection, Direction2d.None, true);
			TransitionBool isDash = new TransitionBool(dashState, database, FSMJargon.isDash, true);
			TransitionBool notDash = new TransitionBool(idleState, database, FSMJargon.isDash, false);
			TransitionCompo moveAndNotDash = new TransitionCompo(moveState, database, new List<FSMTransition>() {moveTriggered, notDash}, true);
			TransitionBool jumpTriggered = new TransitionBool(jumpState, database, FSMJargon.TriggerJump, true);
			TransitionBool onGround = new TransitionBool(idleState, database, FSMJargon.OnGround, true);
			TransitionCompo onGroundStill = new TransitionCompo(idleState, database, new List<FSMTransition>() {onGround, moveStopped}, true);
			TransitionCompo onGroundMove = new TransitionCompo(moveState, database, new List<FSMTransition>() {onGround, moveTriggered}, true);
			TransitionValue<int> meleeTriggered = new TransitionValue<int>(meleeState, database, FSMJargon.MeleeCount, 0, false);
			TransitionValue<int> meleeStopped = new TransitionValue<int>(idleState, database, FSMJargon.MeleeCount, 0, true);
			
			idleState.AddTransition(moveAndNotDash);
			idleState.AddTransition(meleeTriggered);
			idleState.AddTransition(jumpTriggered);
			idleState.AddTransition(isDash);
			
			moveState.AddTransition(moveStopped);
			moveState.AddTransition(meleeTriggered);
			moveState.AddTransition(jumpTriggered);
			moveState.AddTransition(isDash);

			dashState.AddTransition(notDash);
			
			jumpState.AddTransition(onGroundStill);
			jumpState.AddTransition(onGroundMove);
			
			meleeState.AddTransition(meleeStopped);

			currentState = idleState;
		}
	}

}