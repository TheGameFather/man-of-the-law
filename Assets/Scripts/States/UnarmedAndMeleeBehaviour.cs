﻿using UnityEngine;
using System.Collections;
using FSM;
using Platformer2d;

namespace Samurai {

	public class UnarmedAndMeleeBehaviour : FSMBehaviour {

		protected override void Start () {
			base.Start ();

			Helper.InitMeleeFSMDatabase(_database);
			Helper.InitUnarmedFSMDatabase(_database);

			// Melee
			MeleeFSM meleeFSM = new MeleeFSM(_database);
			FSMContainerState meleeContainerState = new FSMContainerState(_database, meleeFSM);
			meleeContainerState.defaultState = meleeFSM.idleState;
			_fsm.AddState(meleeContainerState);

			// Unarmed
			UnarmedFSM unarmedFSM = new UnarmedFSM(_database);
			FSMContainerState unarmedContainterState = new FSMContainerState(_database, unarmedFSM);
			unarmedContainterState.defaultState = unarmedFSM.idleState;
			_fsm.AddState(unarmedContainterState);


			TransitionBool notArmed = new TransitionBool(unarmedContainterState, _database, FSMJargon.isArmed, false);
			TransitionBool armed = new TransitionBool(meleeContainerState, _database, FSMJargon.isArmed, true);

			meleeContainerState.AddTransition(notArmed);
			unarmedContainterState.AddTransition(armed);

			_fsm.currentState = unarmedContainterState;
		}

	}

}