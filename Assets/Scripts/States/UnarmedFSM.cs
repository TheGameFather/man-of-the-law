using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using FSM;
using Platformer2d;

namespace Samurai {

	public class UnarmedFSM : FSMMachine {
		public IdleState idleState {get; set;}
		public MoveState moveState {get; set;}
		public JumpState jumpState {get; set;}

		public UnarmedFSM (FSMDatabase database) : base () {
			idleState = new IdleState(database);
			idleState.animationPart = DollPartType.Body;
			idleState.idleAnimation = SConstants.ANIMATION_UNARMED_IDLE;
			AddState(idleState);

			moveState = new MoveState(database);
			moveState.animationPart = DollPartType.Body;
			moveState.moveForwardAnimation = SConstants.ANIMATION_UNARMED_SPRINT;
			AddState(moveState);

			jumpState = new JumpState(database);
			jumpState.animationPart = DollPartType.Body;
			jumpState.enterJumpAnimation = SConstants.ANIMATION_UNARMED_JUMP_TRANSITION;
			jumpState.jumpAnimation = SConstants.ANIMATION_UNARMED_JUMP;
			jumpState.fallAnimation = SConstants.ANIMATION_UNARMED_FALL;
			jumpState.exitJumpAnimation = SConstants.ANIMATION_UNARMED_JUMP_TRANSITION;
			AddState(jumpState);

			TransitionValue<Direction2d> moveTriggered = new TransitionValue<Direction2d>(moveState, database, FSMJargon.MoveDirection, Direction2d.None, false);
			TransitionValue<Direction2d> moveStopped = new TransitionValue<Direction2d>(idleState, database, FSMJargon.MoveDirection, Direction2d.None, true);
			TransitionBool jumpTriggered = new TransitionBool(jumpState, database, FSMJargon.TriggerJump, true);
			TransitionBool onGround = new TransitionBool(idleState, database, FSMJargon.OnGround, true);
			TransitionCompo onGroundStill = new TransitionCompo(idleState, database, new List<FSMTransition>() {onGround, moveStopped}, true);
			TransitionCompo onGroundMove = new TransitionCompo(moveState, database, new List<FSMTransition>() {onGround, moveTriggered}, true);

			idleState.AddTransition(moveTriggered);
			idleState.AddTransition(jumpTriggered);
			
			moveState.AddTransition(moveStopped);
			moveState.AddTransition(jumpTriggered);
			
			jumpState.AddTransition(onGroundStill);
			jumpState.AddTransition(onGroundMove);

			currentState = idleState;
		}
	}

}