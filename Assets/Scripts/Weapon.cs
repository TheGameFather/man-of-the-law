﻿using UnityEngine;
using System.Collections;

public class Weapon : MonoBehaviour {

	public DamageDealer dealer;
	public int damage;

	// Camera
	public bool shakeCamera;
	private CameraShake _cameraShake;

	void Start () {
		dealer.SignalDealDamage += DealDamage;


		_cameraShake = Camera.main.GetComponent<CameraShake>();

	}
	
	void Update () {
	
	}

	private void DealDamage (DamageReceiver receiver) {
		if (shakeCamera) {
			_cameraShake.Shake();
		}
	}
}
